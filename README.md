# EventGhost Plug-In to Control Denon Receivers

## Overview and Installation
This project contains an EventGhost plugin to control Denon receivers via TCP.
It wraps a fork of the [denonavr](https://github.com/scarface-4711/denonavr)
library that I created that enables backward compatibility Python 2.7.  This
fork can be found at 
[https://github.com/Ben-Mathews/denonavr](https://github.com/Ben-Mathews/denonavr).

This plugin assumes that denonavr and all its dependencies are installed to the
local system.  As of April 4, 2020 this was working on Windows 10 with:
 1) Install EventGhost 0.5.0 RC6 (EventGhost_0.5.0-rc6_Setup.exe)
 2) Install Python Stackless 2.7.12 (python-2.7.12150-stackless.msi)
 3) Install [my fork that enables Python 2.7 compatibility](https://github.com/Ben-Mathews/denonavr) 
 (clone and then use `pip install .`)
 4) pip install html (this was required for denonavr to work but wasn't automatically installed)

This worked with an AVR-X4000.  I expect it should work with similar gear, but
have not tested it with anything other than the AVR-X4000

# Functionality

I have only implemented the functionality that I need for my home theater.
Extending functionality to expose other parts of the 'denonavr' interface
should be relatively easy.  All functionality is parameterized in terms of
receiver zones, so there is no zone-specific functionality.

I have noticed that it is necessary in some cases to add short (0.25 - 
1.0 second) waits inbetween successive commands to the receiver from 
EventGhost.

Supported Functionality:
* Setting Power
* Controlling Volume
* Setting Receiver Inputs

# Configuration
After adding the plug-in to EventGhost the IP address and zone configuration 
must be set.  Right-click on the plug-in and select configure to specify
the configuration. 
![alt text](doc/images/ip_config.png "IP Configuration")  
