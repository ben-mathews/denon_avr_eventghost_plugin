# -*- coding: utf-8 -*-
"""EventGhost Denon Receiver Controller

This project contains an EventGhost plugin to control Denon receivers via TCP.
It wraps a fork of the 'denonavr' library that I created that enables backward
compatibility Python 2.7.  This fork can be found at
https://github.com/Ben-Mathews/denonavr.

This plugin assumes that denonavr and all its dependencies are installed to the
local system.  As of April 4, 2020 this was working on Windows 10 with:
 1) Install EventGhost 0.5.0 RC6 (EventGhost_0.5.0-rc6_Setup.exe)
 2) Install Python Stackless 2.7.12 (python-2.7.12150-stackless.msi)
 3) Install my fork that enables Python 2.7 compatibility (clone and then
    use `pip install .`)
 4) pip install html (this was required for denonavr to work but wasn't
    automatically installed)

This worked with an AVR-X4000.  I expect it should work with similar gear, but
have not tested it with anything other than the AVR-X4000

"""

import eg
import wx
import denonavr

eg.RegisterPlugin(
    name = "Denon TCP/IP Controller",
    guid = "{CF1EA81D-7B59-42F7-8055-C62D82DFAD15}",
    author = "Ben Mathews",
    version = "0.0.1",
    kind = "external",
    description = "Provides TCP/IP Control of Denon Devices via the 'denonavr' Python package"
)


def zone_num_to_name(zone_num):
    """
    Converts numeric zone number to dictionary name
    :param zone_num: Zone number (1 - 3)
    :type zone_num: int
    :return: string to access zone dictionary
    """
    if zone_num == 1:
        zone_name = 'Main'
    elif zone_num == 2 or zone_num == 3:
        zone_name = 'Zone' + str(zone_num)
    else:
        raise Exception('Unhandled zone number: ' + zone_num)
    return zone_name


def zone_name_to_num(zone_name):
    """
    Convertrs a zone name into zone number (1 - 3)
    :param zone_name: Zone name (expecting this to be a string with exactly
                    one numeric: 1, 2, or 3)
    :type zone_name: str
    :return: Zone number (int)
    """
    if zone_name.find('1') > -1 or zone_name.lower().find('main') > -1:
        zone_num = 1
    elif zone_name.find('2') > -1:
        zone_num = 2
    elif zone_name.find('3') > -1:
        zone_num = 3
    else:
        raise Exception('Unhandled zone name: ' + zone_name)
    return zone_num


class DenonAVRPlugin(eg.PluginBase):

    def __init__(self):
        """
        Constructor for DenonAVRPlugin
        """
        self.ip_address = None
        self.num_zones = None
        self.device = None
        self.receiver_input_names = []

        self.AddAction(SetPowerState)
        self.AddAction(SetVolume)
        self.AddAction(AdjustVolume)
        self.AddAction(SetInput)

        # Define Main Zone - Later define Zones 2 and 3 in __start__ if needed
        mainZone = self.AddGroup(
            "Main Zone",
            "Actions for Main Zone"
        )


    def __start__(self, ip_address, num_zones):
        self.ip_address = ip_address
        self.num_zones = num_zones
        print 'Starting DenonAVR with device ' + ip_address + ' with ' \
              + str(num_zones) + ' zones'
        self.initialize_receiver_connection()
        self.receiver_input_names = self.get_input_list()


    def initialize_receiver_connection(self):
        print('Initializing connection to receiver with ' + str(self.num_zones) + ' zones')
        if self.num_zones == 1:
            self.device = denonavr.DenonAVR(self.ip_address, name="Main Zone")
        elif self.num_zones == 2:
            zones = {'Zone2': 'Zone 2'}
            self.device = denonavr.DenonAVR(self.ip_address,
                                            name="Main Zone",
                                            add_zones=zones)
        elif self.num_zones == 3:
            zones = {'Zone2': 'Zone 2', 'Zone3': 'Zone 3'}
            self.device = denonavr.DenonAVR(self.ip_address,
                                            name="Main Zone",
                                            add_zones=zones)
        else:
            raise Exception('Unhandled number of zones: ' + str(self.num_zones))
        self.device.update()


    def get_input_list(self):
        #self.initialize_receiver_connection()
        self.device.update()
        return self.device.input_func_list


    def Configure(self, ip_address="192.168.9.40", num_zones=3):
        panel = eg.ConfigPanel()
        ip_address_ctrl = panel.TextCtrl(ip_address)
        zones_ctrl = panel.SpinIntCtrl(num_zones, max=3)

        st1 = panel.StaticText("Host:")
        st2 = panel.StaticText("Number of Zones (including Main Zone):")

        eg.EqualizeWidths((st1, st2))
        IPBox = panel.BoxedGroup(
            "Receiver Settings",
            (st1, ip_address_ctrl),
            (st2, zones_ctrl),
        )

        panel.sizer.Add(IPBox, 0, wx.EXPAND)

        while panel.Affirmed():
            panel.SetResult(
                ip_address_ctrl.GetValue(),
                zones_ctrl.GetValue(),
            )


    def set_power_state(self, zone_num, power_state):
        """
        Sets power to the specified zone number to on
        :param zone_num: Zone number (1 - 3)
        :type zone_num: int
        :param power_state: String representing power state to
                            set ('on' or 'off')
        :return: void
        """
        zone_name = zone_num_to_name(zone_num)
        #self.initialize_receiver_connection()
        if power_state.lower() == 'on':
            self.device.zones[zone_name].power_on()
        elif power_state.lower() == 'off':
            self.device.zones[zone_name].power_off()
        else:
            raise Exception('Unhandled power state: ' + power_state)


    def set_volume(self, zone_num, volume):
        """
        Sets volume for the specified zone number
        :param zone_num: Zone number (1 - 3)
        :type zone_num: int
        :param volume: Volume in dB
        :type volume: int or double
        :return: void
        """
        zone_name = zone_num_to_name(zone_num)
        #self.initialize_receiver_connection()
        self.device.zones[zone_name].set_volume(volume)


    def set_input(self, zone_num, input_str):
        """
        Sets input source for specified zone number
        :param zone_num: Zone number (1 - 3)
        :type zone_num: int
        :param input_str: Source name (e.g. 'Blu-ray', 'Favorite Station1',
                        'Favorite Station2', 'Favorite Station3',
                        'Favorite Station4', 'Favorites', 'Flickr',
                        'Internet Radio', 'MEDIA PLAYER', 'Media Server',
                        'PHONO', 'Pandora', 'SiriusXM', or 'Spotify')
        :type input_str: str
        :return: void
        """
        #self.initialize_receiver_connection()
        zone_name = zone_num_to_name(zone_num)
        self.device.zones[zone_name].set_input_func(input_str)


    def adjust_volume(self, zone_num, volume_adjustment_db):
        """
        Sets input source for specified zone number
        :param zone_num: Zone number (1 - 3)
        :type zone_num: int
        :param volumne_adjustment: Relative amount to adjust volume by
                                   (positive or negative)
        :type input_str: float
        :return: void
        """
        #self.initialize_receiver_connection()
        zone_name = zone_num_to_name(zone_num)
        self.device.update()
        volume = self.device.volume
        new_volume = max(min(volume + volume_adjustment_db, 18.0), -80.0)
        self.device.zones[zone_name].set_volume(new_volume)


class SetPowerState(eg.ActionBase):
    """
    Sets power state of specified zone of receiver
    """

    def __init__(self):
        """
        Constructor
        """
        self.zone_ctrl_choices = ['Main Zone', 'Zone 2', 'Zone 3']
        self.power_ctrl_choices = ["Off", "On"]


    def __call__(self, zone_name, power_state_str):
        """
        Callback
        :param zone_index: Index into self.zone_ctrl_choices
        :type zone_index: int
        :param power_state_index: Index into self.power_ctrl_choices
        :type power_state_index: int
        :return: void
        """

        zone_index = zone_name_to_num(zone_name)
        print "Setting " + zone_name + " (index=" + str(zone_index) + \
              ") to Power State " + power_state_str
        self.plugin.set_power_state(zone_num=zone_index,
                                    power_state=power_state_str)


    def GetLabel(self, zone_name, power_state_str):
        """
        Sets the label in EG GUI
        :param zone_index: Index into self.zone_ctrl_choices
        :type zone_index: int
        :param power_state_index: Index into self.power_ctrl_choices
        :type power_state_index: int
        :return: void
        """
        return zone_name + ": Set Power = " + power_state_str


    def Configure(self, zone_index=0, power_state_index=1):
        """
        Configure callback
        :param zone_str:
        :param power_state:
        :return:
        """
        panel = eg.ConfigPanel()

        if self.plugin.num_zones is None:
            num_zones = len(self.zone_ctrl_choices)
        else:
            num_zones = self.plugin.num_zones
        zone_ctrl = panel.ComboBox(
            value=self.zone_ctrl_choices[0],
            choices=self.zone_ctrl_choices[0:num_zones],
        )

        power_ctrl = panel.ComboBox(
            value=self.power_ctrl_choices[1],
            choices=self.power_ctrl_choices
        )

        st1 = panel.StaticText("Zone:")
        st2 = panel.StaticText("Power State:")

        eg.EqualizeWidths((st1, st2))
        IPBox = panel.BoxedGroup(
            "Receiver Settings",
            (st1, zone_ctrl),
            (st2, power_ctrl),
        )
        panel.sizer.Add(IPBox, 0, wx.EXPAND)

        while panel.Affirmed():
            panel.SetResult(
                zone_ctrl.GetValue(),
                power_ctrl.GetValue(),
            )


class SetVolume(eg.ActionBase):
    """
    Sets volume of specified zone on receiver
    """

    def __init__(self):
        """
        Constructor
        """
        self.zone_ctrl_choices = ['Main Zone', 'Zone 2', 'Zone 3']


    def __call__(self, zone_name, volume_db):
        """
        Callback
        :param zone_index: Index into self.zone_ctrl_choices
        :type zone_index: int
        :param volume_db: Volume in dB (-80 to 20)
        :type volume_db: int
        :return: void
        """

        zone_index = zone_name_to_num(zone_name)
        self.plugin.set_volume(zone_num=zone_index, volume=volume_db)


    def GetLabel(self, zone_name, volume_db):
        """
        Sets the label in EG GUI
        :param zone_index: Index into self.zone_ctrl_choices
        :type zone_index: int
        :param power_state_index: Index into self.power_ctrl_choices
        :type power_state_index: int
        :return: void
        """
        return zone_name + ": Set Volume = " + str(volume_db) + " dB"


    def Configure(self, zone_index=0, volume=-20.0):
        """
        Configure callback
        :param zone_str:
        :param power_state:
        :return:
        """
        panel = eg.ConfigPanel()

        if self.plugin.num_zones is None:
            num_zones = len(self.zone_ctrl_choices)
        else:
            num_zones = self.plugin.num_zones
        zone_ctrl = panel.ComboBox(
            value=self.zone_ctrl_choices[0],
            choices=self.zone_ctrl_choices[0:num_zones],
        )

        volume_ctrl = panel.SpinNumCtrl(value=volume,
                                        min=-80.0,
                                        max=25.0,
                                        allowNegative=True)

        st1 = panel.StaticText("Zone:")
        st2 = panel.StaticText("Volume (dB):")

        eg.EqualizeWidths((st1, st2))
        IPBox = panel.BoxedGroup(
            "Receiver Settings",
            (st1, zone_ctrl),
            (st2, volume_ctrl),
        )
        panel.sizer.Add(IPBox, 0, wx.EXPAND)

        while panel.Affirmed():
            panel.SetResult(
                zone_ctrl.GetValue(),
                volume_ctrl.GetValue(),
            )


class AdjustVolume(eg.ActionBase):
    """
    Adjusts volume of specified zone on receiver by specified amount
    """

    def __init__(self):
        """
        Constructor
        """
        self.zone_ctrl_choices = ['Main Zone', 'Zone 2', 'Zone 3']


    def __call__(self, zone_name, volume_adjustment_db):
        """
        Callback
        :param zone_index: Index into self.zone_ctrl_choices
        :type zone_index: int
        :param volume_db: Volume in dB (-80 to 20)
        :type volume_db: int
        :return: void
        """

        zone_index = zone_name_to_num(zone_name)
        self.plugin.adjust_volume(zone_num=zone_index,
                                  volume_adjustment_db=volume_adjustment_db)


    def GetLabel(self, zone_name, volume_adjustment_db):
        """
        Sets the label in EG GUI
        :param zone_index: Index into self.zone_ctrl_choices
        :type zone_index: int
        :param power_state_index: Index into self.power_ctrl_choices
        :type power_state_index: int
        :return: void
        """
        if volume_adjustment_db > 0.0:
            return zone_name + ": Volume Adjustment = +" + str(volume_adjustment_db) + " dB"
        elif volume_adjustment_db < 0.0:
            return zone_name + ": Volume Adjustment = " + str(volume_adjustment_db) + " dB"
        else:
            return zone_name + ": Volume Adjustment = " + str(volume_adjustment_db) + " dB"


    def Configure(self, zone_index=0, volume_adjustment_db=1.0):
        """
        Configure callback
        :param zone_str:
        :param power_state:
        :return:
        """
        panel = eg.ConfigPanel()

        if self.plugin.num_zones is None:
            num_zones = len(self.zone_ctrl_choices)
        else:
            num_zones = self.plugin.num_zones
        zone_ctrl = panel.ComboBox(
            value=self.zone_ctrl_choices[0],
            choices=self.zone_ctrl_choices[0:num_zones],
        )

        volume_ctrl = panel.SpinNumCtrl(value=volume_adjustment_db,
                                        min=-20.0,
                                        max=20.0,
                                        allowNegative=True)

        st1 = panel.StaticText("Zone:")
        st2 = panel.StaticText("Volume Adjustment (dB):")

        eg.EqualizeWidths((st1, st2))
        IPBox = panel.BoxedGroup(
            "Receiver Settings",
            (st1, zone_ctrl),
            (st2, volume_ctrl),
        )
        panel.sizer.Add(IPBox, 0, wx.EXPAND)

        while panel.Affirmed():
            panel.SetResult(
                zone_ctrl.GetValue(),
                volume_ctrl.GetValue(),
            )


class SetInput(eg.ActionBase):
    """
    Sets volumne of specified zone on receiver
    """

    def __init__(self):
        """
        Constructor
        """
        self.zone_ctrl_choices = ['Main Zone', 'Zone 2', 'Zone 3']
        self.receiver_input_ctrl_choices = []


    def __call__(self, zone_str, receiver_input_name):
        """
        Callback
        :param zone_index: Index into self.zone_ctrl_choices
        :type zone_index: int
        :param volume_db: Volume in dB (-80 to 20)
        :type volume_db: int
        :return: void
        """
        zone_index = zone_name_to_num(zone_str)
        self.plugin.set_input(zone_num=zone_index,
                              input_str=receiver_input_name)


    def GetLabel(self, zone_str, receiver_input_name):
        """
        Sets the label in EG GUI
        :param zone_index: Index into self.zone_ctrl_choices
        :type zone_index: int
        :param receiver_input_name: Receiver input source name
        :type receiver_input_name: str
        :return: void
        """
        return zone_str + ": Set Input = " + receiver_input_name


    def Configure(self, zone_index=0, receiver_input_name="Blu-ray"):
        """
        Configure callback
        :param zone_str:
        :param power_state:
        :return:
        """
        if len(self.receiver_input_ctrl_choices) == 0:
            self.receiver_input_ctrl_choices = self.plugin.get_input_list()

        panel = eg.ConfigPanel()

        if self.plugin.num_zones is None:
            num_zones = len(self.zone_ctrl_choices)
        else:
            num_zones = self.plugin.num_zones
        zone_ctrl = panel.ComboBox(
            value=self.zone_ctrl_choices[0],
            choices=self.zone_ctrl_choices[0:num_zones],
        )

        receiver_input_ctrl = panel.ComboBox(
            value = self.receiver_input_ctrl_choices[0],
            choices = self.receiver_input_ctrl_choices
        )

        st1 = panel.StaticText("Zone:")
        st2 = panel.StaticText("Input:")

        eg.EqualizeWidths((st1, st2))
        IPBox = panel.BoxedGroup(
            "Receiver Settings",
            (st1, zone_ctrl),
            (st2, receiver_input_ctrl),
        )
        panel.sizer.Add(IPBox, 0, wx.EXPAND)

        while panel.Affirmed():
            panel.SetResult(
                zone_ctrl.GetValue(),
                receiver_input_ctrl.GetValue(),
            )
